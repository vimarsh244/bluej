class static_initialization{
    public static void main(String abc[]){
        byte b =100;
	short s =123;
	int v = 123543;
	int calc = -9876345;
	long amountVal = 1234567891;
	float intrestRate = 12.25f;
	double sineVal = 12345.234d;
	boolean flag = true;
	boolean val = false;
	char ch1 = 88; // code for X
	
	System.out.println("\fStaticaly Intialized.\tPrinting Values--\n\nbyte Value = "+ b);
	System.out.println("short Value = "+ s);
	System.out.println("int Value = "+ v);
	System.out.println("int second Value = "+ calc);
	System.out.println("long Value = "+ amountVal);
	System.out.println("float Value = "+ intrestRate);
	System.out.println("double Value = "+ sineVal);
	System.out.println("boolean Value 1 = "+ flag);
	System.out.println("char Value 1 = "+ ch1);
    }
}