/**
 * @author Vimarsh
 * @version 2.1
 */
/*
 * The program uses input from the user and takes the sum, maximum ,
 * and its minimum value and prints it at last.
 * The input aborts by sending 0.
 * Any developer please tell how to keep tje input values in an array arr[].
 */
import java.io.*;
public class number_series
{
    public static void main(String abc[])throws IOException{
        InputStreamReader read= new InputStreamReader(System.in);
        BufferedReader in= new BufferedReader(read);
        double arr[]={};
        double sum=0;
        double min=999999999,max=-999999,n;
        System.out.println("\fEnter Numbers to find max and min.\nEnter  to abort.");
        do{
            n=Double.parseDouble(in.readLine());
            if(n==0)
                break;
            if(n>max){
                max=n;
            }
            if(n<min){
                min=n;
            }
            sum=sum+n;
            
        }while(n!=0);
        System.out.println("Max: "+max);
        System.out.println("Min: "+min);
        System.out.println("Sum: "+sum);
    }
}
