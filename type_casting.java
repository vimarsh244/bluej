
/**
 * @author vimarsh
 * @version 0.1
 */
public class type_casting
{
    public static void main(int a,float b,char c,double d)
   {
       System.out.println("\fValue of- \n int a: "+a+"\n float b: "+b+"\n char c: "+c+"\n double d: "+d);
       d=(a*b)+c;
       System.out.println("Result of inplicit type casting: "+d);
       d=(char)(a*b)+c;
       System.out.println("Result of explicit type casting to character: "+d);
       d=(int)(a*b)+c;
       System.out.println("Result of explicit type casting to integer: "+d);
   }
}
